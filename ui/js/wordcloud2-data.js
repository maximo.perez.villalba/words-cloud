var words = 
[
	[ 'Tango', 12 ],
	[ 'Alternancia', 6 ],
	[ 'Comunicación', 9 ],
	[ 'Apoyaturas', 6 ],
	[ 'Conexión', 8 ],
	[ 'Abrazo', 11 ],
	[ 'Contacto', 8 ],
	[ 'Los dos centros', 10 ],
	[ 'Posición cero', 8 ],
	[ 'Conceptos', 11 ],
	[ 'Conciencia del movimiento', 10 ],
	[ 'Conectados', 9 ],
	[ 'Contacto', 10 ],
	[ 'Corporal', 8 ],
	[ 'Bailar', 11 ],
	[ 'Movimientos curvos', 10 ],
	[ 'Descarga de peso', 6 ],
	[ 'Desplazamiento', 10 ],
	[ 'Detenciones', 9 ],
	[ 'Didáctica', 11 ],
	[ 'Disociación', 10 ],
	[ 'Encuentro', 9 ],
	[ 'Aprendizaje corporal', 11 ],
	[ 'Equilibrio', 6 ],
	[ 'Espacio', 7 ],
	[ 'Estudio', 10 ],
	[ 'Exploración', 10 ],
	[ 'Fluidez', 9 ],
	[ 'Foco', 8 ],
	[ 'Giros', 9 ],
	[ 'Expresividad', 10 ],
	[ 'Grupal', 8 ],
	[ 'Guía', 9 ],
	[ 'Improvisación', 11 ],
	[ 'PPC', 9 ],
	[ 'Comunidad', 11 ],
	[ 'Interpretacion', 7 ],
	[ 'Juego', 8 ],
	[ 'Juntos', 10 ],
	[ 'Libres', 9 ],
	[ 'Líneas', 7 ],
	[ 'Marco conceptual', 11 ],
	[ 'Mecánica corporal', 10 ],
	[ 'Musicalidad', 9 ],
	[ 'Pareja', 8 ],
	[ 'Patrones', 10 ],
	[ 'Pausas', 9 ],
	[ 'Personalidad', 8 ],
	[ 'Peso del cuerpo', 7 ],
	[ 'Pivot', 10 ],
	[ 'Postura', 9 ],
	[ 'Práctica guiada', 11 ],
	[ 'Presencia · Ausencia', 10 ],
	[ 'Quietud', 7 ],
	[ 'Circuito superior', 9 ],
	[ 'Redondez', 6 ],
	[ 'Ritmo', 11 ],
	[ 'Roles', 9 ],
	[ 'Secuencia', 7 ],
	[ 'Simultaneo', 7 ],
	[ 'Baile social', 9 ],
	[ 'Técnica', 10 ],
	[ 'Torso', 9 ],
	[ 'Trayectorias', 7 ]
];

var factors = 
{
	"desktop": {
		6: 4,
		7: 5,
		8: 6,
		9: 7,
		10: 8,
		11: 9,
		12: 14
	},
	"tablet": {
		6: 3,
		7: 4,
		8: 5,
		9: 6,
		10: 7,
		11: 8,
		12: 10
	},
	"mobile": {
		6: 2,
		7: 2,
		8: 3,
		9: 3,
		10: 4,
		11: 4,
		12: 5
	}

};

var colors = 
{
	6: '#52057F', /*violeta*/
	7: '#FF6908', /*naranja*/
	8: '#4A8594', /*turquesa*/
	9: '#598C14', /*verde*/
	10: '#FF6908', /*naranja*/
	11: '#52057F', /*violeta*/
	12: '#F20A36' /*rojo*/
};

var breaks = {
	"mobile": 0,
	"tablet": 768,
	"desktop": 1024
};